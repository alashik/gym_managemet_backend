const express = require('express')
const app = express()
const PORT =process.env.PORT || 5000
const cors = require('cors')
const jwt = require('jsonwebtoken');
const dotenv= require('dotenv')
const stripe=require('stripe')('sk_test_51OJMNEEAoFFFruGTv9PKEan5JaLsXRLHRgeo46bbrW3xQ4fcBDmgQeT40uFWIqV1XW7wgOcoyYJqFrN8xPf8tIEz00omwTlNIr')

const { MongoClient, ServerApiVersion, ObjectId } = require('mongodb');
const { log } = require('console')

// middleware 
dotenv.config()
app.use(express.json());
app.use(cors());



// const uri = `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASS}@cluster0.o1ht6xv.mongodb.net/?retryWrites=true&w=majority`;

const uri = `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASS}@cluster0.o1ht6xv.mongodb.net/`;

// Create a MongoClient with a MongoClientOptions object to set the Stable API version
const client = new MongoClient(uri, {
  serverApi: {
    version: ServerApiVersion.v1,
    strict: true,
    deprecationErrors: true,
  }
});
async function run() {
  try {
    // Connect the client to the server	(optional starting in v4.7)
    // await client.connect();

    
    const  fitnessTracker = client.db("fitness-tracker");
    const  trainersCollection = fitnessTracker.collection('trainers')
    const  trainersApplyCollection = fitnessTracker.collection('applyTrainer')
    const  usersCollection = fitnessTracker.collection('users') 
    const   bookingsCollection = fitnessTracker.collection('bookings') 
    const    paymentsCollection = fitnessTracker.collection('payments') 

 //jwt token verifiy related api 
 app.post('/jwt', async(req, res)=>{
  const user = req.body;
    const token = jwt.sign(user ,process.env.ACCESS_TOKEN,{expiresIn :'1h'})
    res.send({token})
 })

  // middlewares  
    const verifyToken = (req,res, next) =>{
        // console.log('inside ',req.headers.authorization); 
        if(!req.headers.authorization){
          res.status(401).send('Unauthorized access')
        }
        const token =req.headers.authorization.split(' ')[1]
        jwt.verify(token ,process.env.ACCESS_TOKEN ,(err ,decoded)=>{
            if(err){
              return res.status(403).send({message :'foridden Access'})
            }

            req.decoded = decoded
            next()
        })
        // next()
    }

    // use verify admin after verify token 

      const verifyAdmin = async(req,res,next ) =>{
            const email = req.decoded.email 
            const query = {email :email}
            const user = await usersCollection.findOne(query) 
            const isAdmin = user?.role ==='admin' 
            if(!isAdmin){
              return res.status(403).send({message : 'Forbidden Access'})
            }
            next()
      }


    // users detaile api 
    app.get('/users',verifyToken, verifyAdmin, async(req, res)=>{
      console.log(req.headers);
        const result = await usersCollection.find().toArray()
        // console.log(result);
        res.send(result)
    })
    app.post('/users', async(req,res)=>{
        const query = req.body
        const result = await usersCollection.insertOne(query)
        res.send(result)
    })

    app.delete('/users/:id' , verifyToken ,verifyAdmin, async( req,res)=>{
        const id = req.params.id
        const query = {_id : new ObjectId(id)}
        const result = await usersCollection.deleteOne(query)
        res.send(result)
        
    })

    app.patch('/users/admin/:id' ,verifyToken ,verifyAdmin, async(req,res)=>{
      const id = req.params.id
      const filter = {_id : new ObjectId(id)}
      const updatedDoc = {
        $set :{ role:'admin'}
      }
      const result = await usersCollection.updateOne(filter, updatedDoc)
      res.send(result)
    })

    app.get('/user/admin/:email',verifyToken, async(req, res)=>{
        const email = req.params.email 
        if(email !==req.decoded.email){
          res.status(403).send({message :'Unauthorized access'})
        }
        const query ={email :email}
        const user = await usersCollection.findOne(query) 
        let admin = false 

        if(user){
          admin= user?.role ==='admin'
        }
        res.send({admin})

    })
   

    //get all trainers 
    app.get('/trainers', async (req,res)=>{
        const trainer = await trainersCollection.find().toArray() 
        res.send(trainer)
    })

    //get single trainer details
    app.get('/trainer/:id', async(req, res)=>{
        const id = req.params.id 
        
        const query = {_id:new ObjectId(id)}
        const result = await trainersCollection.findOne(query)
        res.send(result)
    })

    //  add trainer 
    app.post('/trainers', async(req,res)=>{
        const trainer = req.body
        const result = await trainersCollection.insertOne(trainer)
        res.send(result)
    })

    // apply trainer 
    app.post('/applyTrainer', async(req,res)=>{
        const trainer = req.body
        const result = await trainersApplyCollection.insertOne(trainer)
        res.send(result)
    })

    //  delete apply trainer 
    app.delete('/applyTrainer/:id', verifyToken ,verifyAdmin, async(req,res)=>{
        const id = req.params.id 
        const query = {_id:new ObjectId(id)}
        const result = await trainersApplyCollection.deleteOne(query)
        res.send(result)
    })

    app.get('/applyTrainer',verifyToken ,verifyAdmin, async(req, res)=>{
        const result = await trainersApplyCollection.find().toArray()
        res.send(result)
    })
    app.get('/applyTrainer/:id', async(req, res)=>{
        const id= req.params.id 
        const query = {_id :new ObjectId(id)}
        const result = await trainersApplyCollection.findOne(query)
        res.send(result)
    })


    // bookings training 
    app.post('/bookings', async(req,res)=>{
        const query = req.body
        const result = await bookingsCollection.insertOne(query)
       
        res.send(result)
    })
    
    app.get('/bookings', async(req,res)=>{
        const email = req.query.email
        const query = {email:email}
     
        const result = await bookingsCollection.find(query).toArray()
      
        res.send(result)
    })
    
    //payment intend
    app.post('/create-payment-intent' , async(req,res)=>{
      const {price}= req.body;
      // console.log(price);
      const amount = parseInt(price * 100)
      // console.log(amount ,'ammount inside');
      
      const paymentIntent =await stripe.paymentIntents.create({
        amount: amount,
        currency: "usd",
        payment_method_types:['card']
      })
      res.send({
        clientSecret: paymentIntent.client_secret,
      });
    })


    app.post('/payments', async(req,res)=>{
      const payment = req.body
      const paymentResult = await paymentsCollection.insertOne(payment)

      const query = { _id :{
        $in:payment.cartId.map(id => new ObjectId(id))

      }}
      console.log(query);

      const deleteResult =await bookingsCollection.deleteMany(query)

      // console.log(paymentResult);
        res.send({paymentResult ,deleteResult})
    })

    app.get('/payments/:email',verifyToken, async(req,res)=>{
      const query = {email : req.params.email}
      if(req.params.email !== req.decoded.email){
          return res.status(403).send({message :'forbidden access'})
      }
      const result = await paymentsCollection.find(query).toArray()

      res.send(result)
    })


    // statistic 
    app.get('/admin-states', async(req,res)=>{
        const users= await usersCollection.estimatedDocumentCount()
        const trainers= await trainersCollection.estimatedDocumentCount()
        const orders= await paymentsCollection.estimatedDocumentCount()

        const payments = await paymentsCollection.find().toArray()
        const revenue = payments.reduce((total,pay)=>total+pay.price,0)
        res.send({users,trainers ,orders,revenue})
    })

    // Send a ping to confirm a successful connection
    // await client.db("admin").command({ ping: 1 });
    // console.log("Pinged your deployment. You successfully connected to MongoDB!");
  } finally {
    // Ensures that the client will close when you finish/error
    // await client.close();
  }
}
run().catch(console.dir);



app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.listen(PORT, () => {
  console.log(`Server Running  on port ${PORT}`)
})